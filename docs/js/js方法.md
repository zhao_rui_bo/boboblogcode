# 原生 JS 的实例方法

## 防抖：

::: tip 应用场景

1、seach 搜索联想，用户在不断输入输入值时，用防抖来节约请求资源。  
2、windows 触发 resize 的时候，不断的调整浏览器窗口大小会不断的触发这个事件，用防抖来让其只触发这一次。  
3、防止重复提交。  
:::

```js
<button id="bn">快速点击</button>;

// 防抖
function debounce(fn, awai) {
  let timeout;
  return function () {
    clearTimeout(timeout);
    timeout = setTimeout(() => {
      fn.apply(this, arguments);
    }, awai);
  };
}
//因为每一次点击都会清空定时器
function fn() {
  console.log(1);
}
document.getElementById("bn").addEventListener("click", debounce(fn, 2000));
```

## 节流：

::: tip 应用场景

1、鼠标不断点击触发，mousedown(单位时间内只触发一次)。  
2、监听滚动事件，比如是否滑到底部自动加载更多，用 throttle。
:::

```js
// 快速点击的时候，每隔两秒才触发一次事件
function thorrtle(fn, awai) {
  let preven = 0;
  return function () {
    let now = Date.now();
    if (now - preven > awai) {
      fn.apply(this, arguments);
      preven = now;
    }
  };
}
function fn() {
  console.log(1);
}
document.getElementById("bn").addEventListener("click", thorrtle(fn, 2000));
```
