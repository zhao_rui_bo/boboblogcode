# js 基础

## 数据类型

::: tip 基础数据类型 ：
boolean null String Number undefind
:::
::: tip 引用数据类型 ：
function Array Object
:::

## js 判断数据类型的方法

1. **typeof**
2. **instanceof**
3. **constructor**
4. **Object.prototype.toString()**

## 原型链的三者之间的关系

::: tip -
构造函数 -- **prototype** --> 原型对象  
原型对象 -- **constructor** --> 构造函数  
构造函数 -- **new** --> 实例对象  
实例对象 -- **\_\_**proto**\_\_**--> 原型对象  
原型对象 -- **\_\_**proto**\_\_**--> Object

:::

## 闭包

::: tip

### 闭包的优点/作用

1. 实现封装，属性私有化
2. 防止污染全局变量，模块化开发
3. 实现共有变量
4. 可以做缓存

### 闭包的缺点

当闭包函数会导致原有的作用域链不释放,造成内存泄漏

### 什么是闭包

当内部函数被保存到外部时,将会形成闭包
:::
