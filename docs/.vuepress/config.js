module.exports = {
	title: "波波-博客",
	description: "欢迎，大爷儿来玩~",
	cache: false,
	port: "80", // 端口配置 80是默认端口(手动部署服务器时用)
	head: [
		["link", {
			rel: "icon",
			href: `/logo.png`
		}]
	],
	theme: "reco", //启动主题 已安装的有  默认 / reco
	// 设置首页右侧信息栏头像
	authorAvatar: "/log2.png",
	// 设置全局作者姓名
	author: "赵家二少爷",
	selector: ".theme-reco-content :not(a) > img",
	plugins: [
		// 点击图片放大查看的插件,可以省略
		["@vuepress/medium-zoom"],
		// 返回顶部的插件
		["@vuepress/back-to-top"],
		// 看板娘
		[
			"@vuepress-reco/vuepress-plugin-kan-ban-niang",
			{
				theme: [
					"haruto",
					"blackCat",
					"whiteCat",
					"haru1",
					"haru2",
					"koharu",
					"izumi",
					"shizuku",
					"wanko",
					"miku",
					"z16",
				],
				clean: false,
			},
		],
		// 动态title，就是浏览器上的头像
		[
			"dynamic-title",
			{
				showIcon: "/logo.png",
				showText: "(/≧▽≦/)咦！又好了！",
				hideIcon: "/logo.png",
				hideText: "(●—●)喔哟，崩溃啦！",
				recoverTime: 2000,
			},
		],
	],
	themeConfig: {
		// 主题样式
		type: "blog",
		// valine 评论设置
		valineConfig: {
			appId: "HphnXJO6NChHtY50Di9q039D-MdYXbMMI", // your appId
			appKey: "eGw2p5VfR348442nSkdL3ta6", // your appKey
		},
		// 友情连接
		friendLink: [{
			title: "vuepress-theme-reco",
			desc: "一款vuepress的主题",
			logo: "https://vuepress-theme-reco.recoluan.com/icon_vuepress_reco.png",
			link: "https://vuepress-theme-reco.recoluan.com",
		},],
		nav: [{
			text: "首页",
			link: "/",
			icon: "reco-home"
		},
		{
			text: "前端",
			link: "/web/",
			icon: "reco-document"
		}, // 前端
		{
			text: "后端",
			link: "/backend/",
			icon: "reco-document"
		}, // 后端
		{
			text: "前端配置",
			link: "/webconfig/",
			icon: "reco-document"
		}, // 后端
		{
			text: "React框架",
			link: "/react/",
			icon: "reco-document"
		}, // react
		{
			text: "原生JS",
			icon: "reco-category",
			items: [{
				text: "js基础",
				link: "/js/"
			},
			{
				text: "js高级",
				link: "/jsg/"
			},
			],
		},
		{
			text: "时间轴",
			link: "/timeline/",
			icon: "reco-date"
		},
		{
			text: "其它",
			link: "/other/",
			icon: "reco-suggestion"
		}, // 其它
		],
		sidebar: {
			// 其它
			"/other/": [{
				title: "实用小妙招",
				collapsable: true,
				children: [
					"/other/电脑安装环境变量",
					"/other/gitee上的项目",
					"/other/遇到的难点加解决方法",
					"/other/skill",
					"/other/Toollinks",
					"/other/招数"
				],
			},],
			//react
			"/react/": [{
				title: "react",
				collapsable: true,
				children: ["/react/baiscuse", "/react/react_hooks", "/react/redux", "/react/mobx", "/react/reactPitts"],
			},],
			// 后端
			"/backend/": [{
				title: "nodeJs",
				collapsable: true, //是否折叠侧边栏
				children: [
					"/backend/node/nodejs",
					"/backend/node/nodejs_express",
					"/backend/node/nodejs_fs",
				],
			},
			{
				title: "mongoDB",
				collapsable: true, //是否折叠侧边栏
				children: [
					"/backend/mongoDB",
					"/backend/nodeAndmongoose",
					"/backend/BasicADMS",
				],
			},
			{
				title: "MySQL",
				collapsable: true, //是否折叠侧边栏
				children: [
					"/backend/mysql/navicat_Premium",
					"/backend/mysql/MySQL_install",
					"/backend/mysql/mysql_语句",
					"/backend/mysql/mysql_封装",
				],
			},
			{
				title: "html5-webSocket",
				collapsable: true,
				children: [
					"/backend/socket/socketio_basic_use",
				],
			},
			],
			//前端
			"/web/": [{
				title: "请求数据方式",
				collapsable: true,
				children: [
					"/web/axios",
					"/web/fetch请求封装"
				],
			},
			{
				title: "mockjs教程",
				collapsable: true,
				children: ["/web/mockjs"],
			},
			{
				title: "vue教程",
				collapsable: true,
				children: [
					"/web/vue/vue_create",
					"/web/vue/vue2_x",
					"/web/vue/vue2_router",
					"/web/vue/vuex",
					"/web/vue/vue3_vite",
					"/web/vue/vue3_componsition-api",
					"/web/vue/vue3_router",
					"/web/vue/vue3_vuex",
					"/web/vue/vue3_vite_ts",
					"/web/vue/vue3_i_element-plus",
					"/web/vue/vuex_持久化存储插件",
					"/web/vue/error",
				],
			},
			{
				title: "ES6教程",
				collapsable: true,
				children: [
					"/web/es6/es6Array",
					"/web/es6/es6_async",
					"/web/es6/array_fun",
				],
			},
			{
				title: "微信小程序",
				collapsable: true,
				children: [
					"/web/wx/wx_01",
					"/web/wx/wx_02",
					"/web/wx/wx_app",
					"/web/wx/wx_user",
					"/web/wx/wx_request",
				],
			},
			{
				title: 'TypeScript',
				collapsable: true,
				children: [
					"/web/ts/基础入门"
				]
			}
			],
			"/webconfig/": [{
				title: "uni-app教程",
				collapsable: true,
				children: [
					"/webconfig/uni_app/uniApp",
					"/webconfig/uni_app/第三方登录",
					"/webconfig/uni_app/跨端兼容",
					"/webconfig/uni_app/uni_run微信开发者工具",
					"/webconfig/uni_app/uni_config",
					"/webconfig/uni_app/uni开发注意点",
				],
			},
			{
				title: "vuePress教程",
				collapsable: true,
				children: [
					"/webconfig/vuepress",
					"/webconfig/becareful",
					"/webconfig/build",
					"/webconfig/链接",
				],
			},
			{
				title: "git命令",
				collapsable: true,
				children: ["/webconfig/git", "/webconfig/git_config"],
			},
			{
				title: "npm",
				collapsable: true,
				children: ["/webconfig/npm"],
			},
			{
				title: "前端跨域",
				collapsable: true,
				children: ["/webconfig/web跨域"],
			},
			{
				title: "插件配置",
				collapsable: true,
				children: [
					"/webconfig/插件/plug_in_unit",
					"/webconfig/插件/vscode_插件",
				],
			},
			],
			// 原生JS
			"/js/": [{
				title: "原生JS基础",
				collapsable: true,
				children: [
					"/js/js基础",
					"/js/js_array",
					"/js/js方法",
					"/js/js_ifpcisapp",
				],
			},],
			"/jsg/": [{
				title: "原生JS高级",
				collapsable: true,
				children: ["/jsg/js_ajax"],
			},],
		},
		// serviceWorker: {
		//   updatePopup: true,
		// },
		// markdown设置
		markdown: {
			// 显示代码块行号
			lineNumbers: true,
		},
	},
};
