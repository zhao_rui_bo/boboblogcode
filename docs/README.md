---
home: true
heroImage: /log2.png
heroImageStyle:
  {
    maxHeight: "200px",
    display: block,
    margin: "6rem auto 1.5rem",
    borderRadius: "50%",
    boxShadow: "0 5px 18px rgba(0,0,0,0.2)",
  }
---

<style>
.home .hero img{
    width:230px;
    margin: 0.5rem auto 0.5rem !important;
    margin-top:0 !important;
    margin-bottom:0 !important;
}
.home .hero h1{
    margin:0!important;
}
</style>
