## 搭建步骤

### 安装 nodeJs(省略)

::: warning 警告
注意事项 请确保你的 Node.js 版本 >= 8。
:::

### 1.创建一个文件夹

```html
'自定义'
```

### 2.生成一个 package.json

```html
命令：npm init - y
```

### 3 全局安装 vuePress

**官方说已经不再推荐全局安装 VuePress**

```html
命令：npm install vuepress -D
```

### 4.配置 package.json 的命令

把这段代码放在 package.json 的 script 的脚本里

```js
"dev": "vuepress dev docs",
"build": "vuepress build docs"
```

### 5 在根目录中创建一个 docs 的文件夹

```html
docs
```

### 6 配置默认主题 docs/README.md

在 docs 中创建一个 README.md 文件，然后写入一下内容：↓

```html
---
home: true
heroImage: /hero.png
actionText: 快速上手 →
actionLink: /zh/guide/
features:
  - title: 简洁至上
    details: 以 Markdown 为中心的项目结构，以最少的配置帮助你专注于写作。
  - title: Vue驱动
    details: 享受 Vue + webpack 的开发体验，在 Markdown 中使用 Vue 组件，同时可以使用 Vue 来开发自定义主题。
  - title: 高性能
    details: VuePress 为每个页面预渲染生成静态的 HTML，同时在页面被加载的时候，将作为 SPA 运行。
footer: MIT Licensed | Copyright © 2018-present Evan You
---
```

### 7 配置导航 docs/.vuepress/config.js

docs 中创建一个.vuepress 文件夹，在从.vuepress 文件夹中创建一个 config.js，然后写入一下内容：↓

```js
module.exports = {
  title: "Hello VuePress",
  description: "Just playing around",
};
```

创建一个导航 [链接](https://vuepress.vuejs.org/default-theme-config/#navbar)

### 7 配置侧边栏 docs/.vuepress/config.js

docs 中创建一个.vuepress 文件夹，在从.vuepress 文件夹中创建一个 config.js，然后写入一下内容：↓

```js
themeConfig: {
		nav: [
			{ text: '首页', link: '/' },
			{ text: '教程', link: '/web/' },
		],
		sidebar: {
			'/web/': [
				'',//表示README.md
			],
		}
	}
}
```

### 8 在浏览器呈现

能够打开说明已经成功

```html
命令：npm run dev
```

## 文件目录参考

```flow
docs
 ├── .vuepress
 │    ├─ config.js
 |    web
 |    |——README.md
 ├─ package.json
```
