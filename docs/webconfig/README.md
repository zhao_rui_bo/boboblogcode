# 前端相关配置

## [vuePress](vuepress.html)

**Vue 驱动的静态网站生成器**

## [git](git.html)

**Git 是一个开源的分布式版本控制系统，用于敏捷高效地处理任何或小或大的项目**

## [npm](npm.html)

**打造惊人的东西**

## [web 跨域](web跨域.html)

**前端跨域的各种方法**
