# page.json 配置

## 宽屏适配问题 rightWindow

**uni-app 2.9+ 新增`leftWindow`,`topWindow`,`rightWindow `用于解决宽屏适配问题**

`以现有的手机应用为mainWindow，在左、上、右，可以追加新的页面显示窗体`

```js
{
    "pages": [ //pages数组中第一项表示应用启动页，
		{
			"path": "pages/index/index",
			"style": {
				"rightWindow": true
			}
		}
	],
    "rightWindow": {
        "path": "/pages/right/right", // 页面路径
        "style": {
            "width": "100px" // 页面宽度
        },
        "matchMedia": {
            "minWidth": 400 //生效条件，当窗口宽度大于768px时显示
        }
    },
}

```
