# uni-app 基础认识

## 下载 HBuilderX 可视化界面

### HBuilderX[官方 IDE 下载地址](https://www.dcloud.io/hbuilderx.html)

## 创建 uni-app 项目

> 在点击工具栏里的文件 -> 新建 -> 项目：

<img src="/clipboard.png" />

<img src="/uni_001.jpg" />

## 运行 uni-app

1. 浏览器运行：

<img src="/run-chrome.png" />

2. 真机运行：

::: tip 注释 (opppA57 型号)
手机打开开发者模式，用 USB 数据线与电脑连接  
进入【设置】-【关于手机】，然后快速多次点击【版本号】直到屏幕下方显示“您已处于开发者选项”即为已开启开发者选项，此时再进入【设置】-【其他设置】-【开发者选项】【USB 调试】即可进入开发者选项设置页。
:::

::: warning 提示  
如何前面的都已经 ok 的话，显示如下图的话，可能就是数据线的问题，建议换一个数据线，真实测试  
:::

<img src="/uni_002.jpg" />

## manifest.json 配置

<img src="/uni_004.jpg" />
<img src="/uni_005.jpg" />

**配置完之后就是打包发行**  
<img src="/uni_006.jpg" />
<img src="/uni_003.jpg" />
