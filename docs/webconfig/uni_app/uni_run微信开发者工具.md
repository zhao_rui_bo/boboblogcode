# HBuilderX 项目运行到微信开发者工具

## 配置

`点击工具栏的设置，如下图所示：`

<img src='/uniapp_1.png' />

`在运行配置里面设置微信开发者工具的路径,如下图所示:`

<img src='/uniapp_2.png' />

`接着登录微信开发者工具,点击设置下面的安全设置:`

<img src='/uniapp_3.png' />

`开启服务端口,如下图所示:`

<img src='/uniapp_4.png' />

`在运行配置中填写web服务调用URL:`

<img src='/uniapp_5.png' />

`在项目中的manifest.json文件微信小程序配置`

<img src='/uniapp_6.png' />

`点击运行下面的运行到小程序模拟器,如下图所示:`

<img src='/uniapp_7.png' />
