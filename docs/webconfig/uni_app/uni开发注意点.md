# 使用遇到的问题

## navigateBack

- navigateTo, redirectTo 只能打开非 tabBar 页面。
- switchTab 只能打开 tabBar 页面。
- reLaunch 可以打开任意页面。
- 页面底部的 tabBar 由页面决定，即只要是定义为 tabBar 的页面，底部都有 tabBar。
- 不能在 App.vue 里面进行页面跳转。
- H5 端页面刷新之后页面栈会消失，此时 navigateBack 不能返回，如果一定要返回可以使用 history.back()导航到浏览器的其他历史记录。

## 应用生命周期

- 应用生命周期仅可在 App.vue 中监听，在其它页面监听无效。
- onlaunch 里进行页面跳转，如遇白屏报错，请参考https://ask.dcloud.net.cn/article/35942

## onPullDownRefresh 下拉页面刷新
