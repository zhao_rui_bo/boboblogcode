# 配置 git 自动提交代码

1. **创建一个以.sh 结尾的文件，如下：**

```js

```

2. **在 package.json 文件中的 scripts 下添加一行代码如下**

```js
"push":"bash deploy.sh"
```

3. **在 git bash 中提交的时候执行如下命令**

```js
npm run push
```
