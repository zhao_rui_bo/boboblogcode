---
date: 2020-12-13
---

# 插件

## 时间插件 MonmentJs

> 命令：npm install monment -S

**使用**

``` ts

1. 引入

   import moment from 'moment

2. 使用

   let time = moment().format('YYYY-MM-DD HH:mm:ss')
```

## 时间插件 DayJs

> npm install dayjs --save

> **npm 地址**：https://www.npmjs.com/package/dayjs

## 轮播图插件

> 官网地址： https://www.swiper.com.cn/
