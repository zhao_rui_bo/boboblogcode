---
date: 2020-12-13
---

# Vs Code 编辑器

## 功能插件

1. auto close tag
   自动关闭标签，在开始标记的结束括号中键入后，将自动插入结束标记
2. Auto Rename Tag
   自动重命名配对的 HTML / XML 标记
3. Code Runner
   可以直接运行多种语言的代码片段和文件
4. Code Spell Checker
   代码拼写检查工具
5. Debugger for Chrome
   从 VS Code 调试在 Google Chrome 中运行的 JavaScript 代码。
6. JavaScript (ES6) code snippets
   ES6 的代码片段

7. jQuery Code Snippets
   jQuery 代码智能提示
8. Npm Intellisense
   在 import 语句中自动完成 npm 模块引入的代码插件。
9. Path Intellisense
   **https://www.cnblogs.com/-mrl/p/12214252.html（如何配置PHP Intelephense ）**
   自动提示文件路径，支持各种快速引入文件

10. Quokka.js
    Quokka.js 是一个用于 JavaScript 和 TypeScript 的实时运行代码平台。这意味着它会实时运行你输入后的代码，并在编辑器中显示各种执行结果，建议亲自尝试一下。

11. Reactjs code snippets
    快速创建 react 代码的模板

12. Format HTML in PHP
    格式化 php 中的 html 代码结构

13. PHP Server

14. PHP Namespace Resolver

15. PHP Tools for VS Code

16. PHP Formatter

17. PHP Getters & Setters

18. PHP Debug

## Vs Code 搭建 php 的开发环境

> https://blog.csdn.net/qq_44803335/article/details/108806851

> https://blog.csdn.net/baidu_35597657/article/details/109990196

```js
9. Path Intellisense
   **https://www.cnblogs.com/-mrl/p/12214252.html（如何配置PHP Intelephense ）**
   自动提示文件路径，支持各种快速引入文件

12. Format HTML in PHP
    格式化 php 中的 html 代码结构

13. PHP Server

14. PHP Namespace Resolver

15. PHP Tools for VS Code

16. PHP Formatter

17. PHP Getters & Setters

18. PHP Debug
```

## 博主vscode配置
参考链接：https://www.cnblogs.com/fyf-shou/p/13745098.html
```js
{
  "editor.formatOnType": true,
  "vetur.validation.template": false, // vue中template红色波浪线报错处理 
  "eslint.enable": true, // vue中js红色波浪线报错处理
  "editor.formatOnSave": true, //保存时格式化代码
  // 添加 vue 支持
  "eslint.validate": [
    "javascript",
    "typescript",
    "typescriptreact",
    "javascriptreact",
    {
      "language": "vue",
      "autoFix": true
    }
  ],
  "php.suggest.basic": false,
  "window.zoomLevel": 0,
  "workbench.startupEditor": "newUntitledFile",
  "[html]": {
    "editor.defaultFormatter": "vscode.html-language-features"
  },
  "files.autoSave": "off",
  "workbench.colorTheme": "Default Light+",
  "[vue]": {
    "editor.defaultFormatter": "octref.vetur"
  },
  "javascript.updateImportsOnFileMove.enabled": "always",
  "vsicons.dontShowNewVersionMessage": true,
  "breadcrumbs.enabled": true,
  "editor.renderWhitespace": "none",
  "html.format.contentUnformatted": "",
  "extensions.confirmedUriHandlerExtensionIds": [],
  "terminal.integrated.shell.windows": "C:\\WINDOWS\\System32\\cmd.exe", // vscode 终端变成cmd窗口
  "auto-close-tag.activationOnLanguage": [
    "xml",
    "php",
    "blade",
    "ejs",
    "jinja",
    "javascript",
    "javascriptreact",
    "typescript",
    "typescriptreact",
    "plaintext",
    "markdown",
    "vue",
    "liquid",
    "erb",
    "lang-cfml",
    "cfml",
    "HTML (EEx)",
    "HTML (Eex)",
    "plist"
  ],
  "vetur.grammar.customBlocks": {
    "docs": "md",
    "i18n": "json"
  },
  "vetur.format.defaultFormatterOptions": {
    "js-beautify-html": {
      "wrap_attributes": "force-expand-multiline"
    },
    "prettyhtml": {
      "printWidth": 100,
      "singleQuote": false,
      "wrapAttributes": false,
      "sortAttributes": false
    }
  },
  "cSpell.userWords": [
    "a",
    "app",
    "b",
    "console",
    "const",
    "dir",
    "eta",
    "function",
    "function test a b this prototype eta return const app new console dir log",
    "log",
    "new",
    "prototype",
    "return",
    "test",
    "this"
  ],
  "files.associations": {
    "*.cjson": "jsonc",
    "*.wxss": "css",
    "*.wxs": "javascript"
  },
  "emmet.includeLanguages": {
    "wxml": "html"
  },
  "minapp-vscode.disableAutoConfig": true,
  "[javascript]": {
    "editor.defaultFormatter": "vscode.typescript-language-features"
  },
  "rest-client.excludeHostsForProxy": []
}
```