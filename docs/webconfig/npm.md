# npm 托管代码

::: tip 导读
使用 npm 托管自己的代码，可以实现共享使用，如果说 github 是托管项目的地方，那么 npm 就是托管我们写的公共插件的地方
:::

> **npm i -S songyu-audio-mobile** [宋宇的一个基于 vue 的音频播放器插件](https://websong.gitee.io/webBase/npm/01%E4%B8%8A%E4%BC%A0%E4%BB%A3%E7%A0%81%E5%88%B0npm.html#%E5%BC%80%E5%8F%91%E6%9C%AC%E5%9C%B0%E6%8F%92%E4%BB%B6)
