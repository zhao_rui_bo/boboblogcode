# git 命令大全

git checkout -b devName 创建分支

git checkout devName 切换分支

git branch 查看分支

git push --set-upstream origin 分支名 强制推送到线上，线上没有该分支就创建

1.创建本地分支

git checkout -b branchName

2.强制推送到线上，线上么有该分支就创建（第一次）  
git push --set-upstream origin branchName

3.查看本地分支  
git branch -a

4.查看远程仓库  
git remote -v

## 上传代码流程命令

git checkout zrb 切换分支

git status **查看修改过的文件**

git add . **选中所有的文件**

git commit -m'update' **提交到暂存区**

git push origin zrb 传到了自己的分支 主分支没有

**把我的代码合并到主分支上**

git checkout mian 切换到主分支进行合并代码

git merge zrb 主分支与自己的分支进行合并

git pull 合并之后把最新的代码拉取下来，防止覆盖别人的代码

```js
如果出现类似就表示出现了代码冲突，需要在编辑器了进行修改
Auto-merging 111.txt

修改完之后没有执行 git add . ; git commit -m'update'; git push origin main 就会出现以下错误
error: failed to push some refs to 'git@github.com:Wolf-Village/arknights-ui.git
```

git push origin main 传到主分支上，这样主分支就有了我的代码

git checkout zrb 然后在切换到自己的分支进行代码编写

**把主分支的代码合并到我的分支上**

git checkout zrb 切换到自己的分支

git merge main 把代码合并到自己的分支上

git pull 合并之后把最新的代码拉取下来，防止覆盖别人的代码

```js
如果出现类似就表示出现了代码冲突，需要在编辑器了进行修改
Auto-merging 111.txt

修改完之后没有执行 git add . ; git commit -m'update'; git push origin main 就会出现以下错误
error: failed to push some refs to 'git@github.com:Wolf-Village/arknights-ui.git
```

git push origin zrb 然后在上传到自己的分支上
