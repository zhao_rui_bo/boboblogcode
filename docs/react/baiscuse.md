---
date:2021-4-1
---

# 搭建React项目方式

## 一、使用官方脚手架creact-react-app

::: tip
这个脚手架和 vue 的 `vue-cli`是一样的，都可以全局安装，命令： `npm install -g creact-react-app` , 安装好了后，直接使用 `create-react-app <项目名>` 就可以创建一个react 项目了
:::

### 直接使用npx

```js
npx create-react-app <项目名>, 注意，npx 不是拼写错误，是 npm 在 5.2版本之后，推出了一个执行指定代码的一个指令，
```

- 这种方式创建，感觉比较慢，想要快的同学可以使用vite, 接下来的文章中会有相关的描述.

## 二、使用vite 进行搭建

**vite 到现在应该都不陌生，是一个比较新的构建工具。在开发速度的热加载中是快的惊人。**

::: tip
语法： `npm init @vitejs/app my-react-app --template react` [更多指令](https://vitejs.dev/guide/#scaffolding-your-first-vite-project)，查看官方文档，可以把react 替换成 vue vue-ts 或者是下面图片中的任何一个
:::

<img src='/react_1.png' />

```js
npm init @vitejs/app <项目名称> --template react
```

[**相关网站**](https://blog.csdn.net/qq_41499782/article/details/112918551)