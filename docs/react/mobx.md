# mobx

## 安装

1. 运行 npm run eject
2. 如果运行报错执行以下命令：
   git init
   git add .
   git commit -m'update'
   执行完之后在执行 npm run eject

3. npm install mobx --save
4. mobx-react (这个是用来连接 mobx 和 react 的)

5. npm i @babel/plugin-proposal-decorators -D
6. npm i @babel/plugin-proposal-class-properties -D

安装完之后需要进行在 package.json 中进行配置

```js
 "babel": {
    "plugins": [
      [
        "@babel/plugin-proposal-decorators",
        {
          "legacy": true
        }
      ],
      [
        "@babel/plugin-proposal-class-properties",
        {
          "loose": true
        }
      ]
    ],
    "presets": [
      "react-app"
    ]
  },


   "eslintConfig": {
    "parserOptions": {
      "ecmaFeatures": {
        "legacyDecorators": true
      }
    },
    "extends": [
      "react-app",
      "react-app/jest"
    ]
  },
```

本人用的 HBuilderX 编辑器开发

// inject 注入的意思
// observer 把注入进来的 store 进行观察,如果全局变量发生改变，页面就会发生变化

### react Class 组件写法

```js
import React, { Component } from "react";
import { inject, observer } from "mobx-react";
@inject("store")
@observer
class Text extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    let { store } = this.props;
    return <div>{store.o}</div>;
  }
}

export default Text;
```

### 不用修饰器方式的 class 组件写法

```js

import React, { Component } from "react";
import { inject,  } from "mobx-react";
class Text extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    let { store } = this.props;
    return <div>{store.o}</div>;
  }
}

export default inject('store)(observer(Text));

```

### 函数组件写法

```js
import React from "react";
import { inject, observer } from "mobx-react";
const Text = (props) => {
  let { o } = props.store;
  return <div>{o}</div>;
};
export default inject("store")(observer(Text));
```

函数组件写法
https://segmentfault.com/a/1190000025143572

::: tip
补充：

这个我觉得没有必要，因为修饰器属于侵入式，inject、observer 属于高阶组件，直接使用入参控制就可以了，而类组件的话就需要外部的入侵

:::
