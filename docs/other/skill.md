---
title: 网站链接
date: 2020-12-10
---

## 娱乐的一些网站

**阅后即焚网**  
https://sesme.co/

**爱资料工具网站**  
https://www.toolnb.com/

**九蛙工具箱**  
https://www.jiuwa.net/

## 学习网站

**世界数字图书馆**  
https://www.wdl.org/zh/

**华文慕课**  
http://www.chinesemooc.org/

**Deepl 翻译网站**  
https://www.deepl.com/translator
