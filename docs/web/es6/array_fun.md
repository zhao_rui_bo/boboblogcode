# 关于数组的操作的需求

## 随机获取数组中的数据

```js

radomData:function(arr,num){
        //新建一个数组,将传入的数组复制过来,用于运算,而不要直接操作传入的数组;
			var temp_arr = new Array()
			for (let item in arr) {
				temp_arr.push(arr[item])
            }
            // 创建一个新的数组
			var return_arr = new Array()
				for(var i=0;i<num;i++){
					if(temp_arr.length>0){
                    // 在数组中产生一个随机索引
                    var random = Math.floor(Math.random()*temp_arr.length)
                    // 将此随机索引的对应的数组数据进行赋值
                    return_arr[i] = arr[random]
                    // 删掉此索引的数组元素,这时候temp_arr变为新的数组
					temp_arr.splice(random,1)

				}else{
					break;
				}
			}
            // 返回数据
			return return_arr
		}
```
