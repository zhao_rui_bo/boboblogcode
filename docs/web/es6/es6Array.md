# ES6_Array 扩展

## Array.from()

::: tip 导读
Array.from 方法用于将两类对象转为真正的数组：类似数组的对象（array-like object）和可遍历（iterable）的对象（包括 ES6 新增的数据结构 Set 和 Map）。
:::

**下面是一个类似数组的对象，Array.from 将它转为真正的数组。**

```js
let arrayLike = {
  0: "a",
  1: "b",
  2: "c",
  length: 3,
};
// ES5的写法
var arr1 = [].slice.call(arrayLike); // ['a', 'b', 'c']

// ES6的写法
let arr2 = Array.from(arrayLike); // ['a', 'b', 'c']
```

## split

**将一组字符串转成数组**

## splice

**删除数组中的一项或多项**

## slice

**截取数组中的数据**

## join

**将数组转成字符串**

## for of

:::tip
1、for…of 语句在可迭代对象（包括 Array，Map，Set，String，TypedArray，arguments 对象等等）上创建一个迭代循环，调用自定义迭代钩子，并为每个不同属性的值执行语句
:::

```js
for (const value of array) {
  console.log(value); // 保存的是键值
}
```

## for in

::: tip

1、一般用于遍历对象的可枚举属性。以及对象从构造函数原型中继承的属性。对于每个不同的属性，语句都会被执行。
2、不建议使用for in 遍历数组，因为输出的顺序是不固定的。
3、如果迭代的对象的变量值是null或者undefined, for in不执行循环体，建议在使用for in循环之前，先检查该对象的值是不是null或者undefined

:::

```js
for (const key in array) {
  console.log(key); // 保存的是键名
}
```

## 迭代器



