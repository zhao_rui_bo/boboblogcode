# vue2.0版本基础

## 父子组件传值

::: warning 提示
父子组件传值时，父组件传递的参数，数组和对象，子组件接受之后可以直接进行修改，并且会传递给父组件相应的值也会修改。

如果传递的值是字符串，直接修改会报错。

不推荐子组件直接修改父组件中的参数，避免这个参数多个子组件引用，无法找到造成数据不正常的原因
:::

### \$refs 方法

**ref 父组件向子组件传值**

``` ts
// 父组件
<template>
  <div class="about">
    //在子组件上定义一个ref
    <Child ref="Child" />
    <button @click="get" >点击</button>
    </div>
</template>
export default {
  data() {
    return {
      value: 2
    };
  },
  methods: {
    get() {
      // 调用子组件方法
      this.$refs.Child.emit(this.value);
    }
  }
};
</script>
// 子组件 （在methods中直接定义一个方法）
methods: {
    emit(val) {
        //val:父组件传过来的数据
      console.log(val);
    }
}
```

### \$emit 方法

``` ts
// 父组件
在父组件中的自定义的子组件上用v-on监听一个事件，v-on可以用@来代替
<Child v-on:set="headle" />

methods: {
    headle(val) {
       console.log("我是父组件的事件+子组件传过来的数据" + val);
    }
  }

// 子组件
<template>
  <div>
    //自定义一个事件
    <button @click="get" >点击事件</button>
  </div>
</template>

<script>
export default {
  data() {
    return {
      value: 1
    };
  },
  methods: {
    //自定义的事件
    get() {
      //获取父组件的事件，并把数据传到父组件中
      this.$emit("set", this.value);
    }
  }
};
</script>
```

### 兄弟组件传值

## slot 插槽

### 匿名插槽

**父组件**

``` js
< Child >
    <
    p > 我是插槽 < /p> <
    /Child>
```

``` js
< slot > < /slot>
```

### 具名插槽

**父组件**

``` ts
<Child>
  <template v-slot:header>
    <p>我是具名插槽</p>
  </template>
  <p>我是具名插槽</p>
  <template v-slot:footer>
    <p>我是具名插槽</p>
  </template>
</Child>
```

**子组件**

``` ts
  <slot name="header"></slot>
      < slot > < /slot>
  <slot name="footer"></slot>
```

### 作用域插槽

### 第一种写法

**父组件**

``` ts
<Child>
        <template slot-scope="slotvalue">
          <p v-for="(item, index) in slotvalue" :key="index" >
            {{ item.name }}
          </p>
        </template>
      </Child>

子组件

value:是data中的初始数据，可以是对象，可以是数组
 <slot :slotvalue="value"></slot>
```

### 第二种写法

**父组件**

``` ts
  #header是v-slot:header缩写
<Child>
        <template #header="{slotProps}" >
          {{ slotProps }}
        </template>
      </Child>
```

**子组件**

``` ts

<template>
  <div>
    <h1>子组件</h1>
    <h1>
      <slot name="header" :slotProps="val" ></slot>
    </h1>
  </div>
</template>
<script>
export default {
  data() {
    return {
      val: "456"
    };
  }
};
</script>
```

### 动态插槽

``` ts

```

### 给具名作用域插槽设置别名

``` ts
// 这样可以使模板更简洁，尤其是在该插槽提供了多个 prop 的时候。它同样开启了 prop 重命名等其它可能，例如将 user 重命名为 person：
<current-user v-slot="{ user: person }">
  {{ person.firstName }}
</current-user>
```

## vue 生命周期

### beforeCreate 挂载实例之前

::: tip

**适合场景：**  
1.loadding 动画  

2. 骨架屏

:::

### created 实例创建完成

::: tip

**适合场景：**  

1. 比如整个页面都是由后端渲染的，就在这个生命周期中发起请求

:::

### beforeMount 实例挂载之前

::: tip

**适合场景：**  

1. 提示框

:::

### mounted 实例挂载完成

::: tip

**适合场景：**  

1. 前端发送 ajax 请求

:::

### beforeUpdate 更新之前

::: tip

**适合场景：**  

1. 询问用户是否要进行此操作

:::

### updated 更新完成

### beforeDestroy 实例销毁之前

::: tip
**适合场景：**  

1. 一个弹窗，比如说：用户要离开或者退出登录，提示用户是否离开

:::

### destroyed 实例销毁完成

::: tip

**适合场景：**  

1. 删除常占内存  
2. 清除定时器

:::

## 全局API

### nextTick

> https://www.cnblogs.com/qlongbg/p/12896414.html
> https://blog.csdn.net/qq_24147051/article/details/105774852
> https://blog.csdn.net/zhouzuoluo/article/details/84752280?utm_medium=distribute.pc_relevant.none-task-blog-2~default~BlogCommendFromMachineLearnPai2~default-1.control&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2~default~BlogCommendFromMachineLearnPai2~default-1.control

## 计算属性和监听器
### computed 计算属性用法
- https://juejin.cn/post/6956407362085191717
在一个计算属性里可以完成各种复杂的逻辑，包括运算、函数调用等，只要最终返回一个结果就可以。除了上例简单的用法，计算属性还可以依赖多个 Vue 实例的数据，只要其中任一数据变化，计算属性就会重新执行，视图也会更新。

`**getter**` 和 `**setter**` 方法
每一个计算属性都包含一个 getter 方法和一个 setter 方法。

如果计算属性后面直接跟一个 function，使用的就是计算属性的默认方法 getter 来读取。例 展示两个购物车的物品总价：
```js
computed: {
    prices: function(){
        var prices = 0
        for(let i=0; i<this.package.length; i++){
            prices += this.package[i].price * this.package[i].count
        }
        return prices
    }
}
```
我们也可以在需要时使用 setter 函数 ， 当手动修改计算属性的值就像修改一个普通数据那样时，就会触发 setter 函数，执行一些自定义的操作。例 显示全名：
```js
computed: {
    fullName: {
        get: function(){
            return this.firstName + ' ' + this.lastName
        },
        set: function(newValue){
            // 传进来的值用逗号分隔，如'Liu,Bei'
            var names = newValue.split(',') // 分隔为数组
            this.firstName = names[0]
            this.lastName = names[1]
        }
    }
}
```

* [参考l链接](https://blog.csdn.net/weixin_33795743/article/details/93200804)

### 一、计算属性缓存

　　计算属性的特性的确很诱人，但是如果在计算属性方法中执行大量的耗时操作，则可能会带来一些性能问题。例如，在计算属性getter中循环一个大的数组以执行很多操作，那么当频繁调用该计算属性时，就会导致大量不必要的运算。

　　在Vue.js 0.12.8版本之前，只要读取相应的计算属性，对应的getter就会重新执行。而在Vue.js 0.12.8版本中，在这方面进行了优化，即只有计算属性依赖的属性值发生了改变时才会重新执行getter。

　　这样也存在一个问题，就是只有Vue实例中被观察的数据属性发生了改变时才会重新执行getter。但是有时候计算属性依赖实时3的非观察数据属性。代码示例如下：
```js
   var vm = new Vue({
      data:{
          welcome:'welcome to join didiFamily'         
       },
      computed:{
           example:function(){
               return Date.now() + this.welcome    
            }       
        }  
   }) 
```
　　我们需要在每次访问example时都取得最新的时间而不是缓存的时间。从Vue.js 0.12.11版本开始，默认提供了缓存开关，在计算属性对象中指定cache字段来控制是否开启缓存。代码示例如下：

```js

var vm = new Vue({
   data:{
       welcome:'welcome to join didiFamily'         
    },
   computed:{
        example:function(){
            //关闭缓存，默认为true
            cache:false,
            get:function(){
             return Date.now() + this.welcome               
             }      
         }       
     }  
}) 
```
:::tip
**设置cache为false关闭缓存之后，每次直接访问vm.example时都会重新执行getter方法。**
:::
### 二、常见问题

- **在实际开发中使用计算属性时，我们会遇到各种各样的问题，以下是搜集到的一些常见问题以及解决方案。**

::: tip
**1、计算属性getter不执行的场景**
　　从前面我们了解到，当计算属性依赖的数据属性发生改变时，计算属性的getter方法就会执行。但是在有些情况下，虽然依赖数据属性发生了改变，但计算属性的getter方法并不会执行。但是在有些情况下，虽然依赖数据属性发生了改变，但计算属性的getter方法并不会执行。
　　当包含计算属性的节点被移除并且模板中其他地方没有再引用该属性时，那么对应的计算属性的getter方法不会执行。代码示例如下：
```js
<div id="example">
        <button @click='toggleShow'>Toggle Show Total Price</button>
        <p v-if="showTotal">Total Price = {{totalPrice}}</p>
    </div>
    new Vue({
        el:'#example',
        data:{
            showTotal:true,
            basePrice:100
        },
        computed:{
            totalPrice:function(){
                return this.basePrice + 1
            }
        },
        methods:{
            toggleShow:function(){
                this.showTotal = !this.showTotal
            }
        }
    })
```
当点击按钮使showTotal为false，此时P元素会被移除，在P元素内部的计算属性totalPrice的getter方法不会执行。但是当计算属性一直出现在模板中时，getter方法还是会被执行

**2、在v-repeat中使用计算属性**

　　有时候从后端获得JSON数据集合后，我们需要对单条数据应用计算属性。在Vue.js 0.12之前的版本中，我们可以在v-repeat所在元素上使用v-component指令。代码示例如下：

```js
<div id="items">
        <p v-repeat="items" vue-component="item">
            <button>{{fulltext}}</button>
        </p>
    </div>

    var items = [
        {number:1,text:'one'},
        {number:2,text:'two'}
    ]
    var vue = new Vue({
        el:'#items',
        data:{
            items:items
        },
        components:{
            item:{
               computed:{
                    fulltext:function(){
                        return 'item' +this.text
                    }
                }, 
            }
        }
    })
```　


**在Vue.js 0.12版本中，Vue.js废弃了v-component指令，所以我们需要使用自定义元素组件来实现在v-repeat中使用计算属性。代码示例如下：**

```js
<div id="items">
        <my-item v-repeat="items" inline-template>
            
        </my-item>
    </div>

    var items = [
        {number:1,text:'one'},
        {number:2,text:'two'}
    ]
    var vue = new Vue({
        el:'#items',
        data:{
            items:items
        },
        components:{
            'my-item':{
               replace:true, 
               computed:{
                    fulltext:function(){
                        return 'item' +this.text
                    }
                }, 
            }
        }
    })
```
:::

### watch 监听器
- https://juejin.cn/post/6954925963226382367

## 路由守卫

## 修饰符
事件修饰符

.stop 阻止事件继续传播
.prevent 阻止标签默认行为
.capture 使用事件捕获模式,即元素自身触发的事件先在此处处理，然后才交由内部元素进行处理
.self 只当在 event.target 是当前元素自身时触发处理函数
.once 事件将只会触发一次
.passive 告诉浏览器你不想阻止事件的默认行为

v-model 的修饰符


.lazy 通过这个修饰符，转变为在 change 事件再同步


.number 自动将用户的输入值转化为数值类型


.trim 自动过滤用户输入的首尾空格


键盘事件的修饰符

.enter
.tab
.delete (捕获“删除”和“退格”键)
.esc
.space
.up
.down
.left
.right

系统修饰键

.ctrl
.alt
.shift
.meta

鼠标按钮修饰符

.left
.right
.middle


## 内置组件

## 指令使用

## 挂载全局

> \$ 与 src 通级创建 utils/index.js

``` ts
export default {
  // 创建一个websocket连接
  createSocket: function () {
    uni.connectSocket({
      url: "ws://localhost:3000/echo",
      success: function (res) {
        console.log("创建成功");
      },
      fail: function (err) {
        console.log(err + "出错了");
      },
    });
  },
};
```

> \$main.js 挂载

``` ts
// 1.引入
import socket from "utils/socket.js";
// 2、挂载到实例上
Vue.prototype.Socket = socket;
```

> \$ 组件中使用

``` ts
this.Socket.createSocket();
```
