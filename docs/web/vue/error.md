---
date: 2021-2-4
---

# 常见问题的解决方案

## 自动忽略 console.log

**在 `main.js` 引入这个函数并执行一次，就可以实现忽略 console.log 语句的效果。**

``` js
export function rewriteLog() {
    console.log = (function(log) {
        return process.env.NODE_ENV == "development" ? log : function() {};
    })(console.log);
}
```

## vue3.0项目导入element-plus样式文件报错

::: tip
用了**vite脚手架**创建了项目，导入element时启动项目一直报错css样式导入失败，将 `main.js` 里的 `import 'element-plus/lib/theme-chalk/index.css'` 注释掉解决报错，但没了样式
:::

### 解决方法

> https://blog.csdn.net/qq_41769047/article/details/111180961

> **强迫症犯了，折腾了一下午最后发现是项目最外层的中文文件名字造成的，是不是很离谱，简直是老王吃花椒，麻了隔壁，大家将中文文件名改为英文就好了**

## vue安装第三方插件安装失效

:::tip
**node_modules删除在重新安装**
:::

## 常见移动web布局适配方法
**.rem文件的导入问题**

我们在做手机端时，适配是必须要处理的一个问题。例如，我们处理适配的方案就是通过写一个rem.js，原理很简单，就是根据网页尺寸计算html的font-size大小，基本上小伙伴们都知道，这里直接附上代码，不多做介绍。

```js
(function(c, d) {
  var e = document.documentElement || document.body,
    a = "orientationchange" in window ? "orientationchange" : "resize",
    b = function() {
      var f = e.clientWidth;
      e.style.fontSize = f >= 750 ? "100px" : 100 * (f / 750) + "px";
    };
  b();
  c.addEventListener(a, b, false);
})(window);
```

这里说下怎么引入的问题，很简单。在main.js中，直接`import './config/rem'`导入即可。import的路径根据你的文件路径去填写

## params传参丢失问题

**`query`传参方式刷新参数不会丢失，`params`传参会丢失**

**解决方法**
:::tip
**如果使用`params`传值的话，在刷新后不让参数丢失 的话，就在`路由配置中这样写`，携带几个参数就要写几个参数`**
:::
```js
标签跳转
<router-link :to="{name: 'Detail', params: { id: 1 }}">前往Detail页面</router-link>

编程式跳转 vue2.x
Click: function () {
   this.$router.push({ name: "Text", params: { id: 1, token: "asnkdas" } });
},

编程式跳转 vue3.x
编程跳转方法一
router.push("/");//不带参数
router.replace("/");//用replace没有历史记录
router.push({ //路由跳转带参数
 path:"/",
 params:{
   id:"2"
  }
})

编程跳转方法二
ctx.$router.push("/");//路由跳转不带参数
ctx.$router.replace("/");//用replace没有历史记录
ctx.$router.push({ //路由跳转带参数
path: "/",
params: {
  id: "2",
 },
});
路由配置
{
		path: "/text/:id/:token/:name",
		name: 'Text',
		component: () =>
			import ("../view/text.vue"),
},
```