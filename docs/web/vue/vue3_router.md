# vue3.0版本路由

## 安装

``` ts
npm install vue-router@4.0.0-beta.9 --save

#OR

npm install vue-router@next
```

**创建一个 router/index.js**

## 使用

``` js
import {
    createRouter,
    createWebHistory
} from "vue-router";

const routerHistory = createWebHistory();
// 普遍写法
const router = createRouter({
    history: routerHistory,
    routes: [{
        path: "/dome",
        name: "dome",
        component: () => import("../view/dome.vue"),
    }, ],
});
// 正则写法
const router = createRouter({
    history: routerHistory,
    routes: [{
            path: "/:dome(\\d+)", //正则匹配
            name: "Dome",
            alias: "/aa", //别名，别名使用时直接使用aa就可以了可以数组['/home1','/home2']
            component: () => import("../view/dome.vue"),
        },
		// 动态路由写法
		{
			 path: "/text/:id",
			 name:'Text',
			 meta: {适合做权限管理，这个对象能在路由守卫中获取到，这里可以任意添加属性，自定义
			    title: '测试'
				is:true
			  },
			  独享路由守卫
			  beforeEnter:(to,from) =>{
				  //to是当用户点击进入当前页面的时候,我们可以进行一些拦截设置
				  //from当来自其他页面进入当前页面的时候，我们也可以进行拦截提示用户
				  alert('我是路由独享守卫！！！')
			  }
			 component: () =>
			 import ("../view/text.vue"),
		 },
        // 404页面配置写法A
        {
            path: "/:w+", //正则匹配
            component: () => import("../view/dome.vue"),
        },
        // 默认匹配路径写法
        {
            path: "/", //正则匹配
            component: () => import("../view/dome.vue"),
        },
    ],
});
export default router;
```

### 表格

|         属性         |      模式      | 作用 |
| :------------------: | :------------: | :--: |
| createWebHashHistory |      hash      | 路由 |
|   createWebHistory   |    history     | 路由 |
| createMemoryHistory  | 带缓存 history | 路由 |

## 获取路由参数和跳转

``` js
import {
    getCurrentInstance 
} from "vue"; //当前组件实例
import {
    useRouter
} from "vue-router";
setup(props, content) {
	获取路由携带的参数方法一
	const { ctx } = getCurrentInstance();//实例化当前组件
	console.log(ctx.$router.currentRoute.value.query.id);获取参数
	
	编程跳转方法一
	ctx.$router.push("/");//路由跳转不带参数
	ctx.$router.replace("/");//用replace没有历史记录
	ctx.$router.push({ //路由跳转带参数
	path: "/",
	query: {
	  id: "2",
	 },
	});
	获取路由携带的参数方法二
    const router = useRouter();
    console.log(router.currentRoute.value.query.id) // 方法和属性 接收参数
	
	编程跳转方法二
	router.push("/");//不带参数
	router.replace("/");//用replace没有历史记录
	router.push({ //路由跳转带参数
	 path:"/",
	 query:{
	   id:"2"
	  }
    })
	
}
```

## 查看路由信息

``` js
import {
    useRoute
} from "vue-router";
setup(props, content) {
    const route = useRoute();
    console.log(route) // 查看当前页面的路由信息  如：route.path
}
```

## 全局前置路由拦截

**可以完美使用 async await 来做异步处理**

* **vue2.0 使用 `next(false)`来阻止跳转**
* **vue3.0 使用 `return false` 来阻止跳转，`return true` 允许跳转**
* **vue3.0 可以用 `async await` 来**

``` js
router.beforeEach(async (to, from, next) => {
	to:即将进入的页面信息
	from:即将要离开的页面信息
	next:它是一个回调函数，
		  一、不传任何东西的话就是允许跳转；
		  二、如果传入一个false就会阻止页面跳转；
		  三、next('/') 或者 next({ path: '/' }) 跳转到不同的地址。你可以向 next 传递任意位置对象，且允许设置诸如 replace: true、name: 'home' 之类的选项以及任何用在 router-link 的 to prop 或 router.push 中的选项。
		  四、next(error) ) 如果传入 next 的参数是一个 Error 实例，则导航会被终止且该错误会被传递给 router.onError() 注册过的回调。
	next:next是可选参数，可写可不写，return false是取消导航，如果返回值为true或者是undefined意味着通过验证
    if (to.path == "/home1") {
        // console.log(to);
        // console.log(from);
        return await canUserAccess(to)
        console.log("已被拦截");
        return false;
    }
});
```

## 全局后置守卫 afterEach
**这个钩子没有`next`函数也不会改变导航本身：**

```js
router.afterEach((to, from) => {
  // ...
})
```

## 组件路由守卫
- beforeRouteEach
- beforeRouteUpdate
- beforeRouteLeave

## 独享路由守卫 beforeEnter
```js
 const routes = [
        {
            path:"/home",
            name:"home",
            component:Home,
            beforeEnter:(to,from) =>{
                //to是当用户点击进入当前页面的时候,我们可以进行一些拦截设置
                //from当来自其他页面进入当前页面的时候，我们也可以进行拦截提示用户
                alert('我是路由独享守卫！！！')
            }
        }
    ]
```

## 新增Api
https://www.jianshu.com/p/9f527389e6cf
小程序加载时间过长
项目优化
小程序技术难点 
