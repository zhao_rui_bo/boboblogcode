---
date:2021-02-13
---

# Vuex 持久化存储插件

## vuex-along

**安装命令：**

``` ts
npm install vuex-along --save 
或
yarn add vuex-along
```

``` ts
import Vue from 'vue'; 
import Vuex from 'vuex'; 
import createVuexAlong from "vuex-along"; 
const moduleA = {
  state: {

    a1: "hello",
    a2: "world",

  }, 
}; 
const store = new Vuex. Store({
  state: {

    count: 0,  

  }, 
  modules: {

    ma: moduleA,

  }, 
  plugins: [

    createVuexAlong({
      name: "xxx", //设置保存的集合名字，避免同站点下的多项目数据冲突
      local: { //存入 localStorage中
        list: ["ma"],
        isFilter: true // 过滤模块 ma 数据
      },
      session: { //存到 sessionStorage 中
        list: ["count"] // 保存 count 和模块 ma 中的 a1 
      }
    })

  ]
}); 
```

## vuex-persistedstate

**安装命令：**

``` ts
npm install vuex-persistedstate --save
```

**使用：**

``` ts
import createPersistedState from 'vuex-persistedstate'
const store = new Vuex. Store({

     // 默认存储到localStorage

  plugins: [createPersistedState()]  
})   
```

 
**存储到 sessionStorage，配置:**

``` ts
import createPersistedState from "vuex-persistedstate"; 
const store = new Vuex. Store({
  plugins: [

    createPersistedState({
      storage: window.sessionStorage
    })

  ]
}); 
```
