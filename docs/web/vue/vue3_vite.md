# Vue3.0 vite脚手架 安装与使用

## 安装脚手架

``` ts
npm install -g create-vite-app
```

## 创建项目

``` ts
cd projectName
create-vite-app projectName
```

## 下载依赖

``` ts
npm install
```

## 运行

``` ts
npm run dev
```

## 挂载路由和 vuex

**可以通过链条的方式继续调用其他的方法**

``` js
import router from "./router/index";
import store from "./store/index";
createApp(App)
    .use(router)
    .use(store)
    .mount("#app");
```

## 全局变量

**整个项目中，都可以 props 接收 username，并使用**

``` js
main.js中设置
createApp(App, {
    user: "123"
}).mount("#app");
组件中获取
props: ["user"],
```

## 支持多个跟标签

``` js
< template >
    <
    div > 123 < /div> <
div > 123 < /div> <
div > 123 < /div> < /
template >
```

## vue 自定义 hooks

``` js
创建一个js文件， 函数名以use开头
import {
    reactive,
    toRefs
} from "vue";
// 这是默认的use开头，来自定义hooks，这是规则
export default function useDome() {
    const data = reactive({
        count: 0,
    });

    function addCount() {
        data.count++;
    }
    return {
        data: toRefs(data), //toRefs这个API就是可以让响应式可以进行结构
        addCount,
    };
}
组件使用
import useD from "./hooks/usedome";
setup(props, content) {
    var {
        data: {
            count
        },
        addCount,
    } = useD(); // 结构reactive，会失去响应式原理
    return {
        count,
        addCount,
    };
},
```

## provide&&inject 上下文对象

**vue2 的上下文对象**

``` js
创建一个provide.vue文件
provide() { //函数的
        return {
            count: 100,
        };
    },
    孙子组件用inject接收
inject: ["count"],
    created() {
        console.log(this.count);
    },
```

**vue3 的上下文对象**

``` js
//1. 创建一个hooks
import {
    inject,
    provide
} from "vue";
const store = Symbol();

export function provideStore(value) {
    provide(store, value); // 这样就提供了provide
}
export default function useStore(defaultStore) {
    //下面就接收provide
    return inject(store, defaultStore); // 接收两个参数，第一个参数是Symbol，第二个是default
}
//2.provide.vue
setup() {
        const data = reactive({
            count: 100,
        });
        provideStore(data); // 提供store
    },
    //3. inject.vue
    setup() {
        const data = useStore();
        console.log(data);
    },
```

## 配置全局方法

**第一步：创建一个.js 文件**

``` js
//这是导出一个对象，对象中可以写多个方法
export default {
    textFun: function() {
        console.log("这里是全局方法");
    },
};
// 导出一个方法
export default function textFun() {
    console.log("这里是全局方法");
}
```

**第二部：在 mian.js 中引入**

``` js
import {
    createApp
} from "vue";
import Api from "./api/config";
const app = createApp(App);
// 用这个挂载
app.config.globalProperties.$Api = Api;
app
    .use(api)
    .use(store)
    .use(router)
    .mount("#app");
```

**第三步：在组件中调用**

``` js
import {
    onMounted,
    getCurrentInstance
} from "vue";
setup() {
    // 用这个函数能够获取到挂载全局的方法
    const {
        ctx
    } = getCurrentInstance();
    onMounted(() => {
        ctx.$Api.textFun();
    });
}
```

## vite.config.js 配置

**在项目中创建一个 vite.config.js 或 vite.config.ts 文件。如果在当前工作目录中找到 Vite，它将自动使用它。**

```js
```

## vue3中使用JSX

> https://juejin.cn/post/6965057432544346143?utm_source=gold_browser_extension