# Vue-cli脚手架创建vue项目

## 升级 Vue Cli

::: tip
Vue CLI 的包名称由 vue-cli 改成了 @vue/cli。 如果你已经全局安装了旧版本的 vue-cli (1.x 或 2.x)，你需要先卸载老版本
:::

``` ts
npm uninstall vue-cli -g
#OR
yarn global remove vue-cli
```

1. **查看当前版本**

``` ts
vue --version
#OR
vue - V
```

2. **安装最新版本**

``` ts
npm install -g @vue/cli
#OR
yarn global add @vue/cli
```

3. **npm 安装 Vue Cli 时可能出现安装失败**

<img src='/vue_1.png' />

**`可能是由于npm不稳定导致，使用cnpm安装可以解决`**

``` ts
cnpm install -g @vue/cli
```

4. **检查是否安装成功**

``` js
vue--version
#OR
vue - V
```

## 构建项目

**`运行以下命令来创建一个新项目：`**

``` js
vue create demo
```

<img src='/vue_2.jpeg' />

``` js
yushe //自定义的安装方案
Default //提供的方案
Manually select features // 手动选择配置，可以将选中的配置添加到自定义配置
```

``` 

选择手动配置，空格选中取消配置，回车确认
```

<img src='/vue_3.jpeg' />

``` html
Choose Vue version // 选择vue版本 Bable // es高版本转换为低版本 TypeScript //
使用TS语法 Progressive Web App (PWA) Support // Google
在2016年提出的概念，2017年落地的web技术。目的就是在移动端利用提供的标准化框架，在网页应用中实现和原生应用相近的用户体验的渐进式网页应用。
Router // 路由 Vuex // 专为 Vue.js 应用程序开发的状态管理模式 CSS Pre-processors
// css预处理器 Linter / Formatter // 代码提示 Unit Testing // 单元测试 E2E
Testing //
端对端测试，属于黑盒测试，通过编写测试用例，自动化模拟用户操作，确保组件间通信正常，程序流数据传递如预期
```

### **选择 vue 版本， `这里使用2.x` **

<img src='/vue_4.jpeg' />

**`是否使用路由的history模式，建议使用`**

<img src='/vue_5.jpeg' />

### **选择一个要用的预处理语言， `这里选的node-sass` **

<img src='/vue_6.jpeg' />

### ESLint 自动化代码格式化检测，分别为，只预防、Airbnb 配置、标注配置、最高配置， `（博主选择ESLint + Prettier）`

<img src='/vue_7.jpeg' />

### 什么时候检查代码， 保存时，和提交时（博主选择在保存时）

<img src='/vue_8.jpeg' />

### 配置文件存放位置 是单独文件还是 package.json 内 `（博主通常选择package.json）`

<img src='/vue_9.jpeg' />

### 是否将本次配置保存，方便下次使用 ` 博主选择 y`

<img src='/vue_10.jpeg' />

** `本人网站` ：** **https://blog.csdn.net/weixin_42283712/article/details/108214994**
