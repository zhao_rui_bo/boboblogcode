---
date: 2021-2-4
---

# vue3.0 vite脚手架 UI库

## Element-Plus 库

> 官网：https://element-plus.gitee.io/#/zh-CN

``` ts
npm 安装命令：
npm install element-plus --save

main.js完整引入
import ElementPlus from 'element-plus';
import 'element-plus/lib/theme-chalk/index.css';
const app = createApp(App)
app.use(ElementPlus)
app.mount('#app')
```

::: tip 提示
如果遇到导入样式报错，请观看这里
https://zhaojiaershao.github.io/web/vue/error.html
:::
