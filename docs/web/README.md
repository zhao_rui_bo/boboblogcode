---
title:技术简介
---

# 前端程序

- ## [axios](axios.html)

### Axios 是一个基于 promise 的 HTTP 库，可以用在浏览器和 node.js 中。

- ## [vue2.x](vue/vue_create.html)

### Vue(类似于 view)是一套用于构建用户界面的渐进式框架

- ## [mocks](mockjs.html)

### 为 API 开发者设计的管理平台

- ## [vuex](vue/vuex.html)

### Vuex 是一个专为 Vue.js 应用程序开发的状态管理模式

- ## [ES6-Array 扩展](es6/es6Array.html) [官网](http://caibaojian.com/es6/)

### ES6 数组的扩展
