# mockjs 使用

## 安装

1.安装

npm install mockjs --save

npm install axios --save

安装 axios 是为了能模拟后台接口。

2.建立目录

**src 文件目录下面建立 mock 文件夹，mock 下建立 index.js 文件**

**src 文件目录下面建立 mock 文件夹，mock 下建立 mock.js 文件**

**src 文件目录下面建立 api 文件夹，api 下建立 index.js 文件**

3.在 main.js 引入 mockjs

<img src='/9.png'/>

4.api.js 文件(放请求)

<img src='/10.png'/>

5.mock 文件的 index.js 文件

```js
// mock/index.js
import Mock from 'mockjs';
// eslint-disable-next-line standard/object-curly-even-spacing
import { sendList, loginApi } from './home';
// 配置ajax请求超时时间
Mock.setup({
  timeout: 1000
});
// 正则 mockjs可以拦截ajax请求 可以拦截匹配到的路径
// 可以一个页面 拆分出来一个js去做请求处理 / 或者一个功能模块拆分成一个js
Mock.mock(/\/list/, sendList);
Mock.mock(/\/login/, loginApi);
```

6.mock 文件的 mock.js 文件

```js
// mock/home.js   只处理首页的请求
function sendList(req) {
  console.log(req.url);
  let str = req.url;
  console.log(queryString(str));

  return { code: 200 };
}

// 处理 url 拿到数据 http://localhost:3000/list?id=1&name=lq  返回对象
function queryString(str) {
  let arr = str
    .split('?')
    .slice(1)
    .join('')
    .split('&');
  let obj = {};
  for (var key of arr) {
    let value = key.split('=');
    obj[value[0]] = value[1];
  }
  return obj;
}
function loginApi(req) {
  console.log(req);
  const { user, pass } = JSON.parse(req.body);
  console.log(user);
  if (user === 'boshao' && pass === '123456') {
    return { mag: '登陆成功' };
  } else {
    return { mag: '登陆失败' };
  }
}
export { sendList, loginApi };
```

7.components 文件的 home.vue 文件

```js
<script>
import { getList, login } from "../api";

export default {
  name: "home",
  computed: {
    ...mapState(["loading"])
  },
  components: {},
  async created() {
    let data = await getList();
    console.log(data);
    let lon = await login("boshao", "123456");
    console.log(lon);
  }
};
</script>
```

```js
//根据环境变量决定是否使用mock进行数据模拟
if(process.env.NODE_ENV=='development') import('./mock')
import() //es6 里面 懒加载
```
