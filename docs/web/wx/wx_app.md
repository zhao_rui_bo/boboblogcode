# 全局构成

## 全局配置 app.json

`app.json 是当前小程序的全局配置，包括了小程序的所有页面路径、界面表现、网络超时时间、底部 tab 等。`

### pages 配置页面

`pages字段` —— 用于描述当前小程序所有页面路径，这是为了让微信客户端知道当前你的小程序页面定义在哪个目录。
window 字段 —— 小程序所有页面的顶部背景颜色，文字颜色定义在这里的
tab 字段—小程序全局顶部或底部 tab

```js
{
    "pages":[
        "pages/index/index", // 默认显示第一个页面
        "pages/about/about"
    ],

}
```

### tabBar 页面配置 `在 app.json 中配置`

```js
{
    "tabBar": {
        "backgroundColor": "", // tabBar背景
        "selectedColor": "#f00", // 选中页面的text的背景颜色
        "list": [
            {
                "pagePath": "pages/home/home", // 页面路径
                "text": "首页",    // title标题
                "iconPath": "./images/home.png", // 没被点击显示的icon
                "selectedIconPath": "./images/home2.png" // 被点击显示的icon
            },
            {
                "pagePath": "pages/serachValue/serachvalue",
                "text": "搜索",
                "iconPath": "./images/serach.png",
                "selectedIconPath": "./images/serach2.png"
            }
        ]
    },
}
```

## debug 模式

可以在开发者工具中开启 debug 模式，在开发者工具的控制台面板，调试信息以 info 的形式给出，其信息有 Page 的注册，页面路由，数据更新，事件触发等。可以帮助开发者快速定位一些常见的问题。

```js
{
    "debug": true,
}
```

## 全部配置

```js
{
  "pages": [ // 路径配置
    "pages/index/index",
    "pages/logs/index"
  ],
  "window": { // 全局头部标题，页面配置比
    "navigationBarTitleText": "Demo"
  },
  "tabBar": { // tabbar配置
    "list": [{
      "pagePath": "pages/index/index",
      "text": "首页"
    }, {
      "pagePath": "pages/logs/index",
      "text": "日志"
    }]
  },
  "networkTimeout": { // 请求超时
    "request": 10000,
    "downloadFile": 10000
  },
  "debug": true, // 能够更好的找到问题
  "navigateToMiniProgramAppIdList": [
    "wxe5f52902cf4de896"
  ]
}
```

## 封装公共方法

1. **创建一个与 pages 文件同级文件夹/utils.js**

```js
// utils.js
function dome(name) {
  console.log(e);
}
function dome2(name) {
  console.log(e);
}

module.exports.dome = dome;
exports.dome2 = dome2;
```

2. **在需要使用这些模块的文件中，使用 require 将公共代码引入**

```js
var common = require('utils.js')

helloMINA: function() {
    common.dome('MINA')
  },

goodbyeMINA: function() {
    common.dome2('MINA')
  }
```

## 文件作用域

在 JavaScript 文件中声明的变量和函数只在该文件中有效；不同的文件中可以声明相同名字的变量和函数，不会互相影响。
