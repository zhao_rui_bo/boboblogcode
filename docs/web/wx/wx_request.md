---
date:2020-12-21
---

# 数据请求

## 请求封装

**基础版**

``` js
let baseurl = 'http://localhost:666'
/*
url :请求地址
meth：默认get
value ：请求参数
*/
export default function(url, meth = 'get', value) {
    return new Promise((reslove, reject) => {
        wx.request({
            url: `${baseurl}/${url}`,
            method: meth,
            data: value,
            success: function(res) {
                reslove(res)
            },
            fail: function() {
                reject('请求失败')
            }
        })
    })
}
```

**挂载使用**

``` js
app.js
import request from './api/release.js'
wx.$request = request
App({
    onLaunch: function() {

    },
    // 全局变量
    globalData: {
        userInfo: null
    }
})

index.js
Page({
    onLoad: function(options) {
        console.log(wx.$request)
    },
})
`
```
