# mongodb 基础使用

::: tip 导读
MongoDB 是一个基于分布式文件存储的数据库。由 C++ 语言编写。 旨在为 WEB
应用提供可扩展的高性能数据存储解决方案。 MongoDB
是一个介于关系数据库和非关系数据库之间的产品，是非关系数据库当中功能最丰富，最像关系数据库的。
:::

## 安装

1. 我们使用社区免费版即可，[下载地址](https://www.mongodb.com/try/download/community)选择 sersion 版本，默认就好，os 选择我们的操作系统，我这里选择 window
2. 双击安装程序，一路 next，**需要注意的时左下角有一个 install mongoDB compass 把它勾掉**，这个是官方给出的一个图像化操作数据库软件，下载非常的慢，暂时不要。
3. 安装好后，在浏览器输入 127.0.0.1:27017 看到一行英文说明安装成功
4. 手动将 mongoDB 添加到**环境变量**，方便我们开发和使用

## 基础使用

- 环境变量添加好以后关闭所有之前的 cmd 窗口，然后重新开启一个 cmd 输入 mongo，就进入了我们的数据库，之所以重新打开 cmd，是因为 cmd 有缓存，**它不会跟随系统去实时更新环境变量**。
- 简单解释一下：mongodb 这个名称，在下面开启服务和关闭服务都会用到，哪儿来的？是我们安装 mongodb 软件的时候，它有个一安装选择目录界面里自带的，实际上我们可以修改成别的名字，这个名字就是它服务的名字

- mongo 命令启动的是 mongo.exe 这是客户端，用来操作数据库的
- mongod.exe 这个是数据库的服务端

## cmd 命令

1. cmd 命令输入 mongo 回车 它启动的就是 mongo.exe
2. db：该命令是查看当前所在的数据库，默认第一个都是 test
3. show dbs：该命令是查看所有数据库
4. use.dbname：选择数据库，就算没有该数据库页可以
5. db.users.insert({name:"赵瑞波",age:"21"})：在数据库里插入一个文档，文档组成的 users 叫数据集合，如果不存在这个集合，当插入数据是会自动生成
6. db.users.find()：查询该集合里所有的文档数据
7. show collections 或 show tables：查看该数据库里的多少集合
8. 【数据库 db】> 【集合 users】> 【文档数据】
9. 删除文档：db.users.remove({'name':"赵瑞波"}) 查询条件
10. 删除集合：db.users.drop()返回 true 就是删除成功，show tables 查看集合
11. 删除数据库：db.dorpDatabase()此时，输入 show dbs 发现我们得 dbname 数据库不见了
12. 退出数据库：ctrl+c 或者 输入 exit
13. 更多 mongo 客户端命令操作可以去[菜鸟教程](https://www.runoob.com/mongodb/mongodb-dropdatabase.html)

## 开启 mongodb

1. window + x 选择 window powerShell (管理员)，输入 net start mongodb 2.必须是管理员模式的命令窗口才可以，否则会报错。
   如果在管理员模式下启动，报错为：
   Error: couldn't connect to server…… connection attempt failed: SocketException: Error connecting to
   需要使用 mongod 重新服务。
   cmd 中：mongod --dbpath D:\MongoDB\Server\3.2\data 然后在 net start mongodb

## 关闭 mongodb

net stop mongodb
