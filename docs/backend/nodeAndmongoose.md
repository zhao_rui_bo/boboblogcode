# nodejs 和 mongoose 完整流程

> **导读**  
> 本例使用 nodejs+mongoose 去开发使用我们的 mongodb 数据库。  
> 我使用的 node v12.14.1、mogngoose 5.8.7  
> 本篇会系统的整理一下在 nodejs 中使用 mongoose 去使用 mongodb 数据库，后一篇会搭配 express 框架来实现具体开发流程

## mongoose 官方文档

> **http://www.mongoosejs.net/docs/**

## mongoose 安装

```
npm i mongoose -S
```

## 基础使用

#### 连接数据库

```js
// 使用mongoose操作数据库
const mongoose = require("mongoose");
//1 连接数据库
连接数据库【这是在没有用户得情况下】 :27017默认端口可以省略
gameinfo 是我们要连接得数据库，没有也没关系，后续插入数据后，它会自动给创建
const app = mongoose.createConnection("mongodb://127.0.0.1:27017/gameinfo ", {
  useNewUrlParser: true,// 新的url解析器
  useUnifiedTopology: true,// 使用新的解释器
});
// app对象是数据库实例，通过不同事件监听得到不同结果
app.on("open", () => {
  console.log("打开成功");
});

app.on("error", err => {
  console.log("打开失败");
});
```

## 创建规则 Schema

#### 什么是 Schema

> Schema 翻译过来是 “架构图”，实际上它得作用更像是模板，也就是规范，有了 Schema 约束，那么操作数据库得人必须要遵守 Schema 规定得数据类型和字段来操作数据，不然就会操作失效甚至报错。

```js
// 假设数据库原始集合文档有这么一条
[{
  name: "赵家二少",
  age: "20"
}]
// 但是插入数据得人写了一条这样的文档进来
{
  id: "王洪刚",
  job:"测试"
}
我们可以知道，mongodb不会对数据做检测，你存入什么类型或格式都是可以得。
但是这样得一条和我们要得name，age字段是不统一得，后续操作也麻烦了。
```

### 规则类型

- String
- Number
- Date
- Boolean
- Buffer
- ObjectId
- Mixed **（mongoose.Schema.Types.Mixed）**
- Array

### 使用 Schema 建规则

```js
const mongoose = require("mongoose");
在mongoose下有一个Schema构造函数
const Schema = mongoose.Schema;
/*
模型规范：
username字段必须是字符串类型
gender字段使用对象配置：
其中type是类型；
default是指，如果你存入得数据里没有该字段就给个默认得，值是unknown；
lowercase 转成小写
required 必须要的字段，不给就报错
validate自定义检验
msg如果校验不通过，则返回该提示
*/
const Schema = new Schema({
  username: String,
  age: Number,
  gender: {
    type: String,
    default: "unknown",
    lowercase: true,//把值转为小写
    required: true,
    msg: "你该不会是个gay?",
    validate: val => {
      if (val && val.length >= 5) {
        return true;
      }
      return false;
    }
  }
},
 // 强制给集合命名
  { collection: "users" }
);
//你要存进去的数据key名字，必须是我规则里定义好的
//如果我们的规则里没有该字段名称的话，存不进去
创建 || 选择 一个集合,必须是数据库对象创建集合
const Books = app.model("books", Schema);
```

**现在我们有了一个模型，用来统一规范我们的集合数据；**

### Schema.add({}) 添加新规则

```js
Schema.add({
  age: "number",
});
```

### Schema.remove([]) 删除规则

```js
RegisterSchema.remove(["username", "password", "rules", "safes"]);
```

```js
Schema.add({
  age: "number",
});
```

## 创建文档

```js
// 连接上数据库
const mongoose = require("mongoose");
const connet = let connet = mongoose.createConnection("mongodb://127.0.0.1/web1810", {
  useNewUrlParser: true, // 新的url解析器
  useUnifiedTopology: true // 使用新的解释器
});
app.on("open", () => {
  console.log("打开成功");
});

app.on("error", err => {
  console.log("打开失败");
});
const Schema = mongoose.Schema({
    username:'string',
    age:'number',
    gender:'string'
})
// 使用model建立集合
const UserModels = connet.model("users", Schema);
// 向users集合里插入一个文档
UserModels.create({
  username: "宋宇",
  age: 20,
  gender: "男"
}).then(res=>{
console.log(res)
})

打印：
{ _id: 5f7be63277ebe74da418b89d, username: '小米', __v: 0 }
```
