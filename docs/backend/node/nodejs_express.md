# Express 框架

## 中间件 router

::: warning 警告

根目录里也引入了 **Express** 框架，两个变量名不能是一样的

:::

```js
router / index.js;
第一步引入express;
var exp = require("express");
var router = exp.Router();
// post写法
router.post("/path", (req, res) => {});
// get写法
router.get("/path", (req, res) => {});
第二步导出;
module.exports = router;
第三步;
根目录的index.js文件内引入;
const users = require("./router/index");
Exapp.use("/users", users); // users：是所有路径的根目录
```

## all 方法

```js
Exapp.all("*", (req, res, next) => {
  // 1.如果请求方式时option就返回一个200
  // 2.当请求的路径不是user/login 就验证token
  if (req.path == "/users/register") {
    next();
  } else if (
    req.path === "/users/login" &&
    req.method === "POST" &&
    req.body.state === 1
  ) {
    next();
  } else {
    // 剩下的全部都要校验token值
    try {
      verinfyToken(req.get("Authorization"));
      next();
    } catch (error) {
      res.json({ code: 0, msg: "你的令牌已过期", data: { token: false } });
    }
  }
});
```
