# nodeJS 基础认识

```js
// 1. 启动一个后端服务
const express = require("express");
const Exapp = express();
const cors = require("cors");
const bodyParser = require("body-parser");
// 1.2 配置post请求
Exapp.use(bodyParser.urlencoded({ extended: true }));
Exapp.use(bodyParser.json());
// 1.3 配置跨域设置
Exapp.use(cors());
// 1.4 开启后端服务端口
Exapp.listen(3000, () => console.log("http://localhost:3000"));

Exapp.get("/", (req, res, next) => {
  var fullUrl = req.protocol + "://" + req.get("host") + req.originalUrl; // 获取url地址
  console.log(fullUrl);
  res.send({ code: 1 }); // 返回给前端页面的数据
});
```

## 汉字转成编码

**encodeURl() 方法**
