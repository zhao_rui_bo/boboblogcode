# 后端程序

# [nodejs](nodejs.html)

一款基于 jsv8 引擎的后端程序，采用 I/O 非阻塞机制处理请求

# [mongoDB](mongoDB.html)

一个非关系型的超简单好用的数据库，和 nodejs 很搭配
