### vuepress 创建一个博客

1.全局安装 vuePress
npm install vuepress -g

2.创建一个文件夹 boshaoboke
3.npm init - y 生成一个 package.json

4.创建文件夹和文件 参考下面的目录

5.配置 package.json 的命令

```js
"dev": "vuepress dev docs",
"build": "vuepress build docs"
```

## 6.配置默认主题 docs/README.md

home: true
heroImage: /hero.png
actionText: 快速上手 →
actionLink: /zh/guide/
features:

- title: 简洁至上
  details: 以 Markdown 为中心的项目结构，以最少的配置帮助你专注于写作。
- title: Vue 驱动
  details: 享受 Vue + webpack 的开发体验，在 Markdown 中使用 Vue 组件，同时可以使用 Vue 来开发自定义主题。
- title: 高性能
  details: VuePress 为每个页面预渲染生成静态的 HTML，同时在页面被加载的时候，将作为 SPA 运行。
  footer: MIT Licensed | Copyright © 2018-present Evan You

---

##7.在 docs/.vuepress/config.js 写这样一段话

```js
module.exports = {
  title: "Hello VuePress",
  description: "Just playing around",
};
```

8.导航栏链接 docs/.vuepress/config.js 写
可以下拉的导航 items

```js
themeConfig: {
		nav: [
			{ text: '首页', link: '/' },
			{ text: 'web', link: '/web/' },
			{
				text: 'js',
				items: [
					{ text: 'js基础', link: '/js/' },
					{ text: 'js高级', link: '/jsg/' },
				]
			}
		],
```

9.让导航可以使用
docs/web/README.md 9.可以下拉的导航

发布到 github
建立文件 deploy.sh
git 仓库名
zhaojiaershao.github.io

## 如果想在文件下的 README.md 显示路由拦截 backend 文件夹中有使用

[mongoDB](mongoDB.html)

## 改变全局

在.vuepress 文件下创建一个 styles 文件，在 styles 文件夹下创建一个 palette.styl 文件；文件写如下：

```js
$accentColor =blue//默认主题颜色
//$textColor = red//默认字体颜色
$borderColor = #eaecef//默认边框颜色
$codeBgColor = #282c34//默认背景颜色
```

### 基本报错

[链接](https://www.cnblogs.com/wangyuehan/p/9864830.html)
